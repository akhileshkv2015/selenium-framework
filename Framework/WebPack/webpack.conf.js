/**
 * Created by AdamHarvey on 19/07/2016.
 */
module.exports = {
    entry: "./entry.js",
    output: {
        path: "",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};