/**
 * Created by AdamHarvey on 22/07/2016.
 */
module.exports = {
    "Ask Lloyds how much mortgage I can get and input my name" : function (Browser) {
        Browser
            .url("https://secure.liquid-contact.com/Player/sterling.aspx?id=FCEDAE96-0D11-405D-8FBC-7561EACA9E42")
            .pause(3000)
            .click('#decision1 > option:nth-child(30)')
            .click('#decision2 > option:nth-child(3)')
            .waitForElementVisible('#routingPanelContainer', 1000)
            .click('#forename')
            .end();

    }
};
