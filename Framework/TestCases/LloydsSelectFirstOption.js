/**
 * Created by AdamHarvey on 21/07/2016.
 */
module.exports = {
    "Select Lloyds First Option" : function (Browser) {
        Browser
            .url("https://secure.liquid-contact.com/Player/sterling.aspx?id=FCEDAE96-0D11-405D-8FBC-7561EACA9E42")
            .pause(3000)
            .click('#decision1 > option:nth-child(3)')
            .end();
    }
};
