/**
 * Created by AdamHarvey on 22/07/2016.
 */
module.exports = {
    "Select RBS Second Drop Down Box" : function (Browser) {
        Browser
            .url("https://secure.liquid-contact.com/Player/sterling.aspx?id=3698C6EC-FB48-4D87-A455-FD45AA346241")
            .pause(3000)
            .click('#decision1 > option:nth-child(3)')
            .click('#decision2 > option:nth-child(2)')
            .end();
    }
};
