/**
 * Created by AdamHarvey on 22/07/2016.
 */
module.exports = {
    "Select Halifax Second Drop Down Box" : function (Browser) {
        Browser
            .url("https://secure.liquid-contact.com/Player/sterling.aspx?id=14A1B58F-5377-4C87-AF61-0E9AD1968FF2")
            .pause(3000)
            .click('#decision1 > option:nth-child(3)')
            .click('#decision2 > option:nth-child(2)')
            .end();
    }
};
