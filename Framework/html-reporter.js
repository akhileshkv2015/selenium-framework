/**
 * Created by AdamHarvey on 21/07/2016.
 */
var HtmlReporter = require('nightwatch-html-reporter');
/* Same options as when using the built in nightwatch reporter option */
var reporter = new HtmlReporter({
    openBrowser: true,
    reportsDirectory: "reports",
    reportFilename: "generatedTestResults.html",
    themeName: 'default',
    hideSuccess: false
});

module.exports = {
    write : function(results, options, done) {
        reporter.fn(results, done);
    }
};